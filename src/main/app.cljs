(ns app
  (:require [reagent.core :as r]
            [reagent.dom :as rdom]
            ["mapbox-gl" :as mbox]))

(def mapbox-token "YOUR-TOKEN")
(set! (.-accessToken mbox) mapbox-token)

(defn init-map []
  (-> (mbox/Map.
       #js {:container "map"
            :style "mapbox://styles/mapbox/streets-v11"
            :center #js [0 39]
            :zoom 8})
      (.addControl
       (mbox/NavigationControl. #js {:showCompass false})
       "bottom-right")))

(defn init []
  (println "Hi")
  (rdom/render
   [:div#map]
   (.getElementById js/document "root"))
  (init-map))
