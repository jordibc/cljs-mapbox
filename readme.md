# Example of use of mapbox from clojurescript

You'll need to substitute `YOUR-TOKEN` for your actual token first in
`src/main/app.cljs`.

Then you want to:

```sh
yarn install
```

and (for example), use `cider-jack-in-cljs` from emacs.
